﻿using System;

namespace Yash_Abstract_Aug11
{
    class Program
    {
        abstract class Triangleclass
        {
            public int val1 = 2, val2 = 4;
            abstract public void Area();
        }
        class Te : Triangleclass
        {
            
            public override void Area()
            {
                double res = 0.5 * val1 * val2;
                Console.WriteLine("Area of triangle" + res);
            }

        }
        static void Main(string[] args)
        {
            Te t = new Te();
            t.Area();

        }
    }
}
