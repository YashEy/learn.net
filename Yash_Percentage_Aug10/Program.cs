﻿using System;

namespace Yash_Percentage_Aug10
{
    class Program
    {
        static void Main(string[] args)
        {
            int percentage = 0;
            Console.WriteLine("Please Enter the Percentage to Get Grade");
            percentage = Convert.ToInt32(Console.ReadLine());
            if (percentage > 90)
            {
                Console.WriteLine("Grade A");
            }
            else if (percentage > 80 && percentage<=90)
            {
                Console.WriteLine("Grade B");
            }
            else
            { 
                    Console.WriteLine("Fail");    
            }
        }
    }
}
