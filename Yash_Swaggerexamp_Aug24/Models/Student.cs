﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Yash_Swaggerexamp_Aug24.Models
{
    public class Student
    {
        public  int StudentId { get; set; }
        public string StudentName{ get; set; }
        public string StudentCourse { get; set; }
    }
}
