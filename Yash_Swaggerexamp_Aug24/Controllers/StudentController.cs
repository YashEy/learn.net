﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Yash_Swaggerexamp_Aug24.IServices;
using Yash_Swaggerexamp_Aug24.Models;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Yash_Swaggerexamp_Aug24.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class StudentController : ControllerBase
    {
        //dependency is here 
        IStudentService _studentsevice;
        public StudentController(IStudentService studentService)
        {
            _studentsevice = studentService;
        }

        // GET: api/StudentController
        [HttpGet]
        public List<Student> Get()
        {
            return _studentsevice.GetStudentList();
        }

        // GET api/StudentController/5
        [HttpGet("{id}")]
        public Student Get(int id)
        {
            return _studentsevice.GetSingleRecord(id);
        }

        // POST api/StudentController
        [HttpPost]
        public List<Student> Post([FromBody] Student student)
        {
            return _studentsevice.Save(student);
        }

        // PUT api/StudentController/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE api/StudentController/5
        [HttpDelete("{id}")]
        public List<Student> Delete(int id)
        {
            return _studentsevice.Delete(id);
        }
    }
}
