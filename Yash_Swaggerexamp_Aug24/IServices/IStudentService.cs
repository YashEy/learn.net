﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Yash_Swaggerexamp_Aug24.Models;

namespace Yash_Swaggerexamp_Aug24.IServices
{
    public interface IStudentService
    {
        List<Student> GetStudentList();//get all the student list

        Student GetSingleRecord(int id); //to get single student id

        List<Student> Save(Student student);//save
        List<Student> Delete(int studentId);//delete
    }
}
