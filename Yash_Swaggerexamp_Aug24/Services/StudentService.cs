﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Yash_Swaggerexamp_Aug24.IServices;
using Yash_Swaggerexamp_Aug24.Models;

namespace Yash_Swaggerexamp_Aug24.Services
{
    public class StudentService : IStudentService
    {
        List<Student> _student = new List<Student>();
        public StudentService()
        {
            for (int i = 1; i <= 9; i++)
            {
                _student.Add(new Student()
                {
                    StudentId = i,
                    StudentName = "StudentName" + i,
                    StudentCourse = "Course" + i
                });
            }
        }

    public List<Student> Delete(int studentId)
        {
            _student.RemoveAll(x => x.StudentId == studentId);
            return _student;
        }

        public Student GetSingleRecord(int id)
        {
            return _student.SingleOrDefault(x=>x.StudentId== id);
            
        }

        public List<Student> Save(Student student)
        {
            _student.Add(student);
            return _student;
        }

        public List<Student> GetStudentList()
        {
            return _student;  
        }
    }
}
