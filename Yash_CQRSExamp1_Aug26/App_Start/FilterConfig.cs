﻿using System.Web;
using System.Web.Mvc;

namespace Yash_CQRSExamp1_Aug26
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}
