using System;
using System.Reflection;

namespace Yash_CQRSExamp1_Aug26.Areas.HelpPage.ModelDescriptions
{
    public interface IModelDocumentationProvider
    {
        string GetDocumentation(MemberInfo member);

        string GetDocumentation(Type type);
    }
}