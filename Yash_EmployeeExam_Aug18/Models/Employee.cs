﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Yash_EmployeeExam_Aug18.Models
{
    public class Employee
    {
        public Guid Employeeid { get; set; }
        public string EmployeeName { get; set; }
        public int EmployeeSalary { get; set; }
    }
}
