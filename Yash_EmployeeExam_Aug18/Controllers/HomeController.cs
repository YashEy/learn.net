﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Yash_EmployeeExam_Aug18.Models;

namespace Yash_EmployeeExam_Aug18.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;

        public HomeController(ILogger<HomeController> logger)
        {
            _logger = logger;
        }

        public IActionResult Index()
        {
            return View();
        }
        public ViewResult Details()
        {

            Employee employee = new Employee()
            {
                Employeeid = Guid.NewGuid(),
                EmployeeName = "Vishal",
                EmployeeSalary = 100000000
            };
            ViewBag.employeedetails = employee;
            ViewData["Employeedet"] = employee;
            return View();
        }
        public IActionResult DetailsResult()
        {
            return View(employees);

        }
        /*ArrayList al = new ArrayList();
        {


            new Employee() { Employeeid = Guid.NewGuid(), EmployeeSalary = 120000, EmployeeName = "Yash" };
            new Employee{ Employeeid = Guid.NewGuid(), EmployeeSalary = 140000, EmployeeName = "Vishal" };
            new Employee() { Employeeid = Guid.NewGuid(), EmployeeSalary = 160000, EmployeeName = "Sai" };

        };
        */
        private static IList<Employee> employees = new List<Employee>() {
                new Employee() { Employeeid = Guid.NewGuid(), EmployeeSalary = 160000, EmployeeName = "Sai" },
                new Employee() { Employeeid = Guid.NewGuid(), EmployeeSalary = 120000, EmployeeName = "Yash" },
                new Employee() { Employeeid = Guid.NewGuid(), EmployeeSalary = 140000, EmployeeName = "Vishal"}
            };
            
       public IActionResult AddEmployee()
        {
            return View();
        }
        [HttpPost]
        public IActionResult AddEmployee(Employee input)
        {
            employees.Add(new Employee { Employeeid = Guid.NewGuid(), EmployeeName = input.EmployeeName });
            return RedirectToAction("Index");
        }
        [HttpPost]

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
