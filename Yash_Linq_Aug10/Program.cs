﻿using System;

namespace Yash_Linq_Aug10
{
    class Program
    {
        static void Main(string[] args)
        {
            
            string[] mystrarr = { "ab", "bh", "sds", "ssd" };
            var stringToFind = "ab";
            var result = Array.Find(mystrarr, mystrarr => mystrarr == stringToFind);
            Console.WriteLine(result);
        }
    }
}
