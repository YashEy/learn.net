﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Yash_Exam2AspAug18.Models;

namespace Yash_Exam2AspAug18.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;

        public HomeController(ILogger<HomeController> logger)
        {
            _logger = logger;
        }

        public IActionResult Index()
        {
            ViewData["abc"] = "Example 1";//#1
            ViewBag.secondexample = "Example 2";//#2
            return View();
        }
        public ViewResult Details()
        {
            Student student = new Student()
            {
                StudentId = 1,
                StudentName = "Yash",
                Gender="Male",
                Section="Section A",
                Grade="A"
            };
            ViewData["Student"] = student;
            return View();
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
