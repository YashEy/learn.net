﻿using System;
using System.Collections;

namespace Yash_Arrex_Aug10
{
    class Program
    {
        static void Main(string[] args)
        {
            ArrayList ar1 = new ArrayList();
            ar1.Add(1);
            ar1.Add("Yash");
            ar1.Add(24.05);

            foreach (var i in ar1)
                Console.Write(i + ",");

            Console.Write("\n");
            ar1.Remove("Ya");
            foreach (var i in ar1)
                Console.Write(i + ",");

            Console.Write("\n");
            ar1.Add("Bhageriya");
            ar1.RemoveAt(0);
            foreach (var i in ar1)
                Console.Write(i + ",");

            Console.Write("\n");
            Console.WriteLine(ar1.Contains("Yash"));
            Console.WriteLine(ar1.IndexOf("Bhageriya"));

        }

    }
}
