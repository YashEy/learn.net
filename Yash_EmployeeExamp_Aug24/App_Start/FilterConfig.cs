﻿using System.Web;
using System.Web.Mvc;

namespace Yash_EmployeeExamp_Aug24
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}
