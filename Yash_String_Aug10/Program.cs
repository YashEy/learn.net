﻿using System;

namespace Yash_String_Aug10
{
    class Program
    {
        static void Main(string[] args)
        {
            string str = "Yash Bhageriya";
            Console.WriteLine(str.Length);
            Console.WriteLine(str.ToLower());
            Console.WriteLine(str.ToUpper());
            Console.WriteLine(str.Trim());
            Console.WriteLine(str.Replace(" ",""));
            Console.WriteLine(str.Remove(3));
        }
    }
}
