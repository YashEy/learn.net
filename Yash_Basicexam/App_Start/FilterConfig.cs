﻿using System.Web;
using System.Web.Mvc;

namespace Yash_Basicexam
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}
