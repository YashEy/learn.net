﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Yash_MVCStudentProject_Aug20.Models
{
    public class Student
    {
        public Guid StudentId { get; set; }
        public string StudentName { get; set; }
        public string Studentphnno { get; set; }
        public string Studentaddr{ get; set; }
        public string Studentemail { get; set; }
        public DateTime StudentDoj { get; set; }
        public string Studentcourse { get; set; }
        public string Studentstream { get; set; }
        public DateTime Studentdob { get; set; }
        public string Studentspecialization { get; set; }



    }
}
