﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Yash_MVCStudentProject_Aug20.Models;

namespace Yash_MVCStudentProject_Aug20.Controllers
{
    public class StudentController : Controller
    {
       /* public IActionResult Index()
        {
            return View();
        }
        public IActionResult Privacy()
        {
            return View();
        } */
        public static IList<Student> student = new List<Student>
        {
            new Student{StudentId=Guid.NewGuid(),StudentName="Yash",Studentaddr="Mamta nursing home",Studentspecialization="Maths",Studentstream="Science",
                        Studentcourse="BE",Studentdob=new DateTime(2015,04,08).Date,StudentDoj=new DateTime(2019,03,02),Studentemail="yasgd@gmail.com",Studentphnno="6871436943"},
                        new Student{StudentId=Guid.NewGuid(),StudentName="Vishal",Studentaddr="Divyasree chamber",Studentspecialization="English",Studentstream="Science",
                        Studentcourse="BTech",Studentdob=new DateTime(2016,05,03).Date,StudentDoj=new DateTime(2020,06,01),Studentemail="yfefwfd@gmail.com",Studentphnno="6789863369"}

        };
        public IActionResult AddStudent()
        {
            return View();
        }
        [HttpPost]
        public IActionResult AddStudent(Student input)
        {
            student.Add(new Student
            {
                StudentId = Guid.NewGuid(),
                StudentName = input.StudentName,
                Studentaddr = input.Studentaddr,
                Studentcourse = input.Studentcourse,
                Studentdob = input.Studentdob,
                StudentDoj = input.StudentDoj,
                Studentemail = input.Studentemail,
                Studentphnno = input.Studentphnno,
                Studentspecialization = input.Studentspecialization,
                Studentstream = input.Studentstream
            });
            return RedirectToAction("StudentDetails");
        }

        /*public IActionResult Delete()
        {
            return View();
        }*/

        [HttpPost]
        public ActionResult Delete(Guid id)
        {
            student.Remove(student.SingleOrDefault(o => o.StudentId == id));
            return RedirectToAction("StudentDetails");
        }
        [HttpGet]
        public ActionResult Edit(Guid id)
        {
            return View(student.Single(o => o.StudentId == id));
        }
        [HttpPost]
        public ActionResult Edit(Guid id,Student input)
        {
            var stud = student.Where(s => s.StudentId == id).SingleOrDefault();
            student.Remove(stud);
            student.Add(new Student
            {
                StudentId = Guid.NewGuid(),
                StudentName = input.StudentName,
                Studentaddr = input.Studentaddr,
                Studentcourse = input.Studentcourse,
                Studentdob = input.Studentdob,
                StudentDoj = input.StudentDoj,
                Studentemail = input.Studentemail,
                Studentphnno = input.Studentphnno,
                Studentspecialization = input.Studentspecialization,
                Studentstream = input.Studentstream
            });
            return View();
        }

        public IActionResult StudentDetails()
        {
            return View(student);
        }

    }
}
