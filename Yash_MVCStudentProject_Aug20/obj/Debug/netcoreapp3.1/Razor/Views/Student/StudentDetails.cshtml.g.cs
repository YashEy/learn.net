#pragma checksum "C:\Users\eystack02\source\repos\Yash_MVCStudentProject_Aug20\Views\Student\StudentDetails.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "10c965b6e1709bedc3713481d08220b7fa79ed53"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_Student_StudentDetails), @"mvc.1.0.view", @"/Views/Student/StudentDetails.cshtml")]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#nullable restore
#line 1 "C:\Users\eystack02\source\repos\Yash_MVCStudentProject_Aug20\Views\_ViewImports.cshtml"
using Yash_MVCStudentProject_Aug20;

#line default
#line hidden
#nullable disable
#nullable restore
#line 2 "C:\Users\eystack02\source\repos\Yash_MVCStudentProject_Aug20\Views\_ViewImports.cshtml"
using Yash_MVCStudentProject_Aug20.Models;

#line default
#line hidden
#nullable disable
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"10c965b6e1709bedc3713481d08220b7fa79ed53", @"/Views/Student/StudentDetails.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"eef39a2682d100737b954329ed2b29569342ef21", @"/Views/_ViewImports.cshtml")]
    public class Views_Student_StudentDetails : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<IEnumerable<Yash_MVCStudentProject_Aug20.Models.Student>>
    {
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_0 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("method", "post", global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        #line hidden
        #pragma warning disable 0649
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperExecutionContext __tagHelperExecutionContext;
        #pragma warning restore 0649
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner __tagHelperRunner = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner();
        #pragma warning disable 0169
        private string __tagHelperStringValueBuffer;
        #pragma warning restore 0169
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __backed__tagHelperScopeManager = null;
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __tagHelperScopeManager
        {
            get
            {
                if (__backed__tagHelperScopeManager == null)
                {
                    __backed__tagHelperScopeManager = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager(StartTagHelperWritingScope, EndTagHelperWritingScope);
                }
                return __backed__tagHelperScopeManager;
            }
        }
        private global::Microsoft.AspNetCore.Mvc.TagHelpers.FormTagHelper __Microsoft_AspNetCore_Mvc_TagHelpers_FormTagHelper;
        private global::Microsoft.AspNetCore.Mvc.TagHelpers.RenderAtEndOfFormTagHelper __Microsoft_AspNetCore_Mvc_TagHelpers_RenderAtEndOfFormTagHelper;
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
            WriteLiteral("\r\n                      <div>\r\n                          ");
#nullable restore
#line 4 "C:\Users\eystack02\source\repos\Yash_MVCStudentProject_Aug20\Views\Student\StudentDetails.cshtml"
                     Write(Html.ActionLink("Add Student","AddStudent"));

#line default
#line hidden
#nullable disable
            WriteLiteral(@"
                          <table width=""150%"" border=""1"">
                              <tr style=""height:fit-content"">
                                  <th>Student Id</th>
                                  <th>Student Name</th>
                                  <th>Student Stream</th>
                                  <th>Student Phn no</th>
                                  <th>Student Dob</th>
                                  <th>Student Doj</th>
                                  <th>Student Email</th>
                                  <th>Student Specialization</th>
                                  <th>Student Address</th>
                                  <th>Student Course</th>
                              </tr>
                          </table>
");
#nullable restore
#line 19 "C:\Users\eystack02\source\repos\Yash_MVCStudentProject_Aug20\Views\Student\StudentDetails.cshtml"
                            
                          foreach (var item in Model)
                          {

#line default
#line hidden
#nullable disable
            WriteLiteral("                          <table width=\"150%\" border=\"1\">\r\n                              <tr style=\"height:fit-content\">\r\n                                  <td>");
#nullable restore
#line 24 "C:\Users\eystack02\source\repos\Yash_MVCStudentProject_Aug20\Views\Student\StudentDetails.cshtml"
                                 Write(item.StudentId);

#line default
#line hidden
#nullable disable
            WriteLiteral("</td>\r\n                                  <td>");
#nullable restore
#line 25 "C:\Users\eystack02\source\repos\Yash_MVCStudentProject_Aug20\Views\Student\StudentDetails.cshtml"
                                 Write(item.StudentName);

#line default
#line hidden
#nullable disable
            WriteLiteral(" &nbsp;&nbsp;</td>\r\n                                  <td>");
#nullable restore
#line 26 "C:\Users\eystack02\source\repos\Yash_MVCStudentProject_Aug20\Views\Student\StudentDetails.cshtml"
                                 Write(item.Studentstream);

#line default
#line hidden
#nullable disable
            WriteLiteral(" &nbsp;&nbsp;</td>\r\n                                  <td>");
#nullable restore
#line 27 "C:\Users\eystack02\source\repos\Yash_MVCStudentProject_Aug20\Views\Student\StudentDetails.cshtml"
                                 Write(item.Studentphnno);

#line default
#line hidden
#nullable disable
            WriteLiteral(" &nbsp;&nbsp;</td>\r\n                                  <td>");
#nullable restore
#line 28 "C:\Users\eystack02\source\repos\Yash_MVCStudentProject_Aug20\Views\Student\StudentDetails.cshtml"
                                 Write(item.Studentdob);

#line default
#line hidden
#nullable disable
            WriteLiteral(" &nbsp;&nbsp;</td>\r\n                                  <td>");
#nullable restore
#line 29 "C:\Users\eystack02\source\repos\Yash_MVCStudentProject_Aug20\Views\Student\StudentDetails.cshtml"
                                 Write(item.StudentDoj);

#line default
#line hidden
#nullable disable
            WriteLiteral(" &nbsp;&nbsp;</td>\r\n                                  <td>");
#nullable restore
#line 30 "C:\Users\eystack02\source\repos\Yash_MVCStudentProject_Aug20\Views\Student\StudentDetails.cshtml"
                                 Write(item.Studentemail);

#line default
#line hidden
#nullable disable
            WriteLiteral(" &nbsp;&nbsp;</td>\r\n                                  <td>");
#nullable restore
#line 31 "C:\Users\eystack02\source\repos\Yash_MVCStudentProject_Aug20\Views\Student\StudentDetails.cshtml"
                                 Write(item.Studentspecialization);

#line default
#line hidden
#nullable disable
            WriteLiteral(" &nbsp;&nbsp;</td>\r\n                                  <td>");
#nullable restore
#line 32 "C:\Users\eystack02\source\repos\Yash_MVCStudentProject_Aug20\Views\Student\StudentDetails.cshtml"
                                 Write(item.Studentaddr);

#line default
#line hidden
#nullable disable
            WriteLiteral(" &nbsp;&nbsp;</td>\r\n                                  <td>");
#nullable restore
#line 33 "C:\Users\eystack02\source\repos\Yash_MVCStudentProject_Aug20\Views\Student\StudentDetails.cshtml"
                                 Write(item.Studentcourse);

#line default
#line hidden
#nullable disable
            WriteLiteral(" &nbsp;&nbsp;</td>\r\n                                  <td>\r\n                                      ");
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("form", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "10c965b6e1709bedc3713481d08220b7fa79ed538615", async() => {
                WriteLiteral("\r\n                                          <input type=\"submit\" value=\"Delete\" class=\"btn btn-primary\" />\r\n                                      ");
            }
            );
            __Microsoft_AspNetCore_Mvc_TagHelpers_FormTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.FormTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_FormTagHelper);
            __Microsoft_AspNetCore_Mvc_TagHelpers_RenderAtEndOfFormTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.RenderAtEndOfFormTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_RenderAtEndOfFormTagHelper);
            BeginAddHtmlAttributeValues(__tagHelperExecutionContext, "action", 1, global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
#nullable restore
#line 35 "C:\Users\eystack02\source\repos\Yash_MVCStudentProject_Aug20\Views\Student\StudentDetails.cshtml"
AddHtmlAttributeValue("", 2026, Url.Action("Delete",new{ id=@item.StudentId}), 2026, 46, false);

#line default
#line hidden
#nullable disable
            EndAddHtmlAttributeValues(__tagHelperExecutionContext);
            __Microsoft_AspNetCore_Mvc_TagHelpers_FormTagHelper.Method = (string)__tagHelperAttribute_0.Value;
            __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_0);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            WriteLiteral("\r\n                                      <a");
            BeginWriteAttribute("href", " href=\'", 2283, "\'", 2337, 1);
#nullable restore
#line 38 "C:\Users\eystack02\source\repos\Yash_MVCStudentProject_Aug20\Views\Student\StudentDetails.cshtml"
WriteAttributeValue("", 2290, Url.Action("Edit",new { id = item.StudentId }), 2290, 47, false);

#line default
#line hidden
#nullable disable
            EndWriteAttribute();
            WriteLiteral(" class=\"btn btn-primary\">Edit</a>\r\n                                      \r\n                                  </td>\r\n                              </tr>\r\n                          </table>\r\n");
#nullable restore
#line 43 "C:\Users\eystack02\source\repos\Yash_MVCStudentProject_Aug20\Views\Student\StudentDetails.cshtml"
                          

                          }
                          

#line default
#line hidden
#nullable disable
            WriteLiteral("                      </div>\r\n");
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<IEnumerable<Yash_MVCStudentProject_Aug20.Models.Student>> Html { get; private set; }
    }
}
#pragma warning restore 1591
