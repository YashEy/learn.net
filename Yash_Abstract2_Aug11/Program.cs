﻿using System;

namespace Yash_Abstract2_Aug11
{
    abstract public class str
    {
        public String str1 = "Hello";
        public String str2 = "World";

        public abstract void joiner();
    }

    public class derived : str
    {
        public override void joiner()
        {
            Console.WriteLine(str1 + " " + str2);
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            derived d = new derived();
            d.joiner();

        }
    }
}
