﻿using Crud2.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Crud2.DataAccess
{
    public interface IDataAccess
    {
        List<Student> GetStudents();
        Student GetStudentById(int id);
        Student AddStudent(object firstName, object lastName, object age);
    }
}
