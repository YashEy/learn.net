﻿using Crud2.Commands;
using Crud2.DataAccess;
using Crud2.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Crud2.Handlers
{
    class AddStudentHandler
    {
        private readonly IDataAccess _data;
        public AddStudentHandler(IDataAccess data)
        {
            _data = data;
        }
        public Task<Student> Handle(AddStudentCommand request, CancellationToken cancellationToken)
        {
            return Task.FromResult(_data.AddStudent(request.firstName, request.lastName, request.age));
        }
    }
}
