﻿using Crud2.Models;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;

namespace Crud2.Commands
{
    class AddStudentCommand
    {
        //public record AddStudentCommand()
        public record AddStudentCommand(string firstName, string lastName, double age) : IRequest<Student>;
    }
}
