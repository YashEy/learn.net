﻿using Crud2.Models;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;

namespace Crud2.Queries
{
    public class GetStudentListQuery : IRequest<List<Student>>
        {
        }
}
