﻿using System;

namespace Yash_RegistrationForm_Aug9
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Please Enter your Registration details here:");
            int id = Convert.ToInt32(Console.ReadLine());
            String name = Console.ReadLine();
            DateTime dob = Convert.ToDateTime(Console.ReadLine());
            char gender = Convert.ToChar(Console.ReadLine());

            Console.WriteLine("\n\nID - " + id);
            Console.WriteLine("NAME - " + name);
            Console.WriteLine("DOB - " + dob.ToString("dd/MM/yyyy"));
            Console.WriteLine("GENDER - " + gender);
        }
    }
}
