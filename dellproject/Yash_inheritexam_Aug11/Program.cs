﻿using System;

namespace Yash_inheritexam_Aug11
{
    class vehicle
    {
        public string name;

    }

    class car : vehicle
    {
        public void visible()
        {
            //name = Console.ReadLine();
            Console.WriteLine("Cars");
        }
    }

    class student
    {
        public string name;

    }

    class cl : student
    {
        public void visible()
        {
            //name = Console.ReadLine();
            Console.WriteLine("student");
        }
    }

    class theatre
    {
        public string name;

    }

    class movies : theatre
    {
        public void visible()
        {
            //name = Console.ReadLine();
            Console.WriteLine("theatre");
        }
    }

    class apps
    {
        public string name;

    }

    class playapp : apps
    {
        public void visible()
        {
            //name = Console.ReadLine();
            Console.WriteLine("apps");
        }
    }

    class books
    {
        public string name;

    }

    class library : books
    {
        public void visible()
        {
            //name = Console.ReadLine();
            Console.WriteLine("books");
        }
    }

    class aeroplane
    {
        public string name;

    }

    class airpts : aeroplane
    {
        public void visible()
        {
            //name = Console.ReadLine();
            Console.WriteLine("aeroplane");
        }
    }

    class food
    {
        public string name;

    }

    class foodcourt : food
    {
        public void visible()
        {
            //name = Console.ReadLine();
            Console.WriteLine("food");
        }
    }

    class cricket
    {
        public string name;

    }

    class sports : cricket
    {
        public void visible()
        {
            //name = Console.ReadLine();
            Console.WriteLine("food");
        }
    }

    class treadmill
    {
        public string name;

    }

    class fitgym : treadmill
    {
        public void visible()
        {
            //name = Console.ReadLine();
            Console.WriteLine("cricket");
        }
    }

    class mobile
    {
        public string name;

    }

    class electronics : mobile
    {
        public void visible()
        {
            //name = Console.ReadLine();
            Console.WriteLine("mobile");
        }
    }

    static class laptop
    {
        public static String name = "Hello"; //cannot read it as class is static

    }

    class mouse
    {
        public int a = 10;
    }

}

namespace Yash_inheritexam_Aug11
{
    class A : mouse //shows error as parent class is sealed 
    {
        public void visible()
        {
            Console.WriteLine(name);        // shows error as variable cant be accessed due to sealed property
        }
    }
}

namespace Yash_inheritexam_Aug11
{
    class Program
    {
        static void Main(string[] args)
        {
            car v = new car(); v.visible();
            cl s = new cl(); s.visible();
            movies m = new movies(); m.visible();
            playapp ap = new playapp(); ap.visible();
            library b = new library(); b.visible();
            airpts ae = new airpts(); ae.visible();
            foodcourt f = new foodcourt(); f.visible();
            sports c = new sports(); c.visible();
            fitgym t = new fitgym(); t.visible();
            electronics me = new electronics(); me.visible();
        }
    }
}