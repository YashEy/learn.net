﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Yash_Exam2AspAug18.Models
{
    public class Student
    {
        public string StudentName { get; set; }
        public int StudentId { get; set; }
        public string Gender { get; set; }
        public string Section { get; set; }
        public string Grade { get; set; }
    }
}
