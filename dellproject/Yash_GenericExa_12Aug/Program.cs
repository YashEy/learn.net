﻿using System;

namespace Yash_GenericExa_12Aug
{
    class Program
    {
        public class SClass
        {
            public void GenericMethod<T1, T2>(T1 Param1, T2 Param2)
            {
                Console.WriteLine($"Param 1: {Param1} with type {Param1.GetType()} & Param 2: {Param2} with type {Param2.GetType()}");
            }

            public void GenericMethod<T1, T2, T3>(T1 Param1, T2 Param2, T3 Param3)
            {
                Console.WriteLine($"Param 1: {Param1} with type {Param1.GetType()} & Param 2: {Param2} with type {Param2.GetType()} & Param 3: {Param3} with type {Param3.GetType()}");
            }
        }


        static void Main(string[] args)
        {
            Console.WriteLine("Generic Example");
            SClass s = new SClass();
            s.GenericMethod(10, 29);
            s.GenericMethod(10.5, 29.5);
            s.GenericMethod("Hello", "World");
            s.GenericMethod("Hello", "World", "Beautiful");
            s.GenericMethod<int, string>(10, "wj");
            s.GenericMethod<int, string, double>(10, "wj", 22.5);

        }
    }

}
