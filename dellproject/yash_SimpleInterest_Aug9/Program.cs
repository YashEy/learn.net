﻿using System;

namespace yash_SimpleInterest_Aug9
{
    class Program
    {
        static void Main(string[] args)
        {
            int principalval;
            int rateval;
            int timeval;
            Console.WriteLine("Enter principal amount");
            principalval = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Enter rate of interest");
            rateval = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Enter tenure");
            timeval = Convert.ToInt32(Console.ReadLine());
            int result = (principalval * rateval * timeval) / 100 ;
            Console.WriteLine("Principal"+ result);
        }
    }
}
