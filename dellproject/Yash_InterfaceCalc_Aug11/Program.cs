﻿using System;

namespace Yash_InterfaceCalc_Aug11
{
    static void Main(string[] args)
    {
        ICalc calc = new Program();
        Interface1 intf1 = new Program();
        try
        {
            calc.add();
            calc.subtract();
            intf1.divide();
            intf1.multiply();
            //Console.ReadLine();
        }
        catch(Exception e)
        {
            Console.WriteLine(e.Message);
        }

    }
    class Program : ICalc, Interface1
    {
        public void add()
        {
            throw new NotImplementedException();
            Console.WriteLine("Adding");
        }

        public void divide()
        {
            throw new NotImplementedException();
            Console.WriteLine("Divide");
        }

        public void multiply()
        {
            throw new NotImplementedException();
            Console.WriteLine("Multiplying");
        }

        public void subtract()
        {
            throw new NotImplementedException();
            Console.WriteLine("Subtracting");
        }
    }
}
