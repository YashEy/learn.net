﻿using System;

namespace Yash_Generics_Aug12
{
    public class SomeClass
    {
        public void GenericMethod<T>(T Param1,T Param2)
        {
            Console.WriteLine($"Parameter 1: {Param1} : Parameter 2: {Param2}");
        }
    }
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Generic method exa.");
            SomeClass s = new SomeClass();
            //while calling the method we need to specify the type
            s.GenericMethod<int>(10, 20);
            s.GenericMethod(10.5, 20.5);
            s.GenericMethod("Hi", "there");
            Console.ReadLine();
        }
    }
}
