﻿using System;
using System.Collections;

namespace Yash_IEnum2_Aug12
{
    class Program
    {
        static void Main(string[] args)
        {
            ArrayList list = new ArrayList();
            list.Add("Details:");
            list.Add("Name");
            list.Add("Marks");

            Array list2 = new Array();
            

            list.CopyTo(list2 ,0);
            
            IEnumerator iEnum = list.GetEnumerator();
            
            string msg = "";
            while (iEnum.MoveNext())
            {
                msg += iEnum.Current.ToString() + "\n";
            }
            Console.WriteLine(msg);

        }
    }
    }

