﻿using System;

namespace Yash_controlstruc_Aug10
{
    class Program
    {
        static void Main(string[] args)
        {
            int sum = 0;
            for (int i = 1; i <= 6; i++)
            {
                Console.WriteLine("Please Enter the marks of"+i+"Subject");
                int marks = Convert.ToInt32(Console.ReadLine());
                
                sum = sum + marks;
            }
            int percentage = sum / 6;
            Console.WriteLine(percentage);
            if (percentage > 90)
            {
                Console.WriteLine("Grade A");
            }
            else if (percentage > 80 && percentage <= 90)
            {
                Console.WriteLine("Grade B");
            }
            else
            {
                Console.WriteLine("Fail");
            }
        }
    }
}
