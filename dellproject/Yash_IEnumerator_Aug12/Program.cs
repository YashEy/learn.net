﻿using System;
using System.Collections;

namespace Yash_IEnumerator_Aug12
{
    class Program
    {
        static void Main(string[] args)
        {
            ArrayList list = new ArrayList();
            list.Add(123);
            list.Add("test");
            list.Add(2.39);
            IEnumerator iEnum = list.GetEnumerator();
            string msg = "";
            while (iEnum.MoveNext())
            {
                msg += iEnum.Current.ToString() + "\n";
            }
            Console.WriteLine(msg);

        }
    }
}
