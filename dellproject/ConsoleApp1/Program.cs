﻿using System;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            string name;
            Console.WriteLine("Enter your name:");
            name = Console.ReadLine();

            string result = name.Substring(name.Length - 5);
            Console.WriteLine(result);
        }
    }
}
