﻿using System;

namespace Yash_EMployee_Aug12
{
    class Program
    {
        public class Employee
        {
            int empid = 0;
            string empname = string.Empty;

            //alternate appproach public int EId { get; set; }
            public int E_Id
            {
                get { return empid; }
                set { empid = value; }
            }
            public string Emp_Name
            {
                get { return empname; }
                set { empname = value; }
            }
            public Employee(int id, string name)
            {
                E_Id = id;
                Emp_Name = name;
            }
            public

            static void Main(string[] args)
            {
                Console.WriteLine("Enter employee id:");
                Employee myclaas = new Employee(9,"yash");
                Console.WriteLine(myclaas.E_Id);

            }
        }
    }
}
