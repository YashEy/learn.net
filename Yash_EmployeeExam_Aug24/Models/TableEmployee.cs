﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Yash_EmployeeExam_Aug24.Models
{
    public partial class TableEmployee
    {
        [Required]
        public int EmpId { get; set; }
        [Required]
        public string EmpName { get; set; }
        [Required]
        public string Address { get; set; }
        [Required]
        public string Designation { get; set; }
        [Range(400,50000)]
        public decimal? Salary { get; set; }
        [Required]
        public DateTime? JoiningDate { get; set; }
    }
}
