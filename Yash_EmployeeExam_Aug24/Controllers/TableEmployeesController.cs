﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Yash_EmployeeExam_Aug24.Models;

namespace Yash_EmployeeExam_Aug24.Controllers
{
    public class TableEmployeesController : Controller
    {
        private readonly CompanyDBContext _context;

        public TableEmployeesController(CompanyDBContext context)
        {
            _context = context;
        }

        // GET: TableEmployees
        public async Task<IActionResult> Index()
        {
            return View(await _context.TableEmployee.ToListAsync());
        }

        // GET: TableEmployees/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var tableEmployee = await _context.TableEmployee
                .FirstOrDefaultAsync(m => m.EmpId == id);
            if (tableEmployee == null)
            {
                return NotFound();
            }

            return View(tableEmployee);
        }

        // GET: TableEmployees/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: TableEmployees/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("EmpId,EmpName,Address,Designation,Salary,JoiningDate")] TableEmployee tableEmployee)
        {
            if (ModelState.IsValid)
            {
                _context.Add(tableEmployee);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(tableEmployee);
        }

        // GET: TableEmployees/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var tableEmployee = await _context.TableEmployee.FindAsync(id);
            if (tableEmployee == null)
            {
                return NotFound();
            }
            return View(tableEmployee);
        }

        // POST: TableEmployees/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("EmpId,EmpName,Address,Designation,Salary,JoiningDate")] TableEmployee tableEmployee)
        {
            if (id != tableEmployee.EmpId)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(tableEmployee);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!TableEmployeeExists(tableEmployee.EmpId))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(tableEmployee);
        }

        // GET: TableEmployees/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var tableEmployee = await _context.TableEmployee
                .FirstOrDefaultAsync(m => m.EmpId == id);
            if (tableEmployee == null)
            {
                return NotFound();
            }

            return View(tableEmployee);
        }

        // POST: TableEmployees/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var tableEmployee = await _context.TableEmployee.FindAsync(id);
            _context.TableEmployee.Remove(tableEmployee);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool TableEmployeeExists(int id)
        {
            return _context.TableEmployee.Any(e => e.EmpId == id);
        }
    }
}
