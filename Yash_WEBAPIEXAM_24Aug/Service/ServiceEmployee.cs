﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Yash_WEBAPIEXAM_24Aug.IService;
using Yash_WEBAPIEXAM_24Aug.Models;

namespace Yash_WEBAPIEXAM_24Aug.Service
{
    public class ServiceEmployee : IEmployeeService
    {
        List<Employee> _employeeservice = new List<Employee>();
        public ServiceEmployee()
        {
            for (int i = 1; i <= 99; i++)
            {
                _employeeservice.Add(new Employee()
                {
                    EmployeeId = i,
                    Employeeadd="Add"+ i,
                    EmployeeName="Name"+i
                });
            }

        }
           
        public List<Employee> Delete(int employeeId)
        {
            _employeeservice.RemoveAll(d => d.EmployeeId == employeeId);
            return _employeeservice;
        }

        public List<Employee> GetEmployeesDetails()
        {
            return _employeeservice;
          
        }

        public Employee GetStudentRecord(int id)
        {
            return _employeeservice.SingleOrDefault(s => s.EmployeeId == id);
        }

        public List<Employee> Save(Employee employee)
        {
            _employeeservice.Add(employee);
            return _employeeservice;
        }
    }
}
