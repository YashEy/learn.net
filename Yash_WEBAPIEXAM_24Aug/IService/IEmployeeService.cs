﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Yash_WEBAPIEXAM_24Aug.Models;
using Yash_WEBAPIEXAM_24Aug.Service;

namespace Yash_WEBAPIEXAM_24Aug.IService
{
    public interface IEmployeeService
    {
        List<Employee> GetEmployeesDetails();
        List<Employee> Save(Employee employee);
        List<Employee> Delete(int EmployeeId);
        Employee GetStudentRecord(int id);
    }
}
