﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Yash_WEBAPIEXAM_24Aug.IService;
using Yash_WEBAPIEXAM_24Aug.Models;
using Yash_WEBAPIEXAM_24Aug.Service;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Yash_WEBAPIEXAM_24Aug.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class EmployeeController : ControllerBase
    {
        IEmployeeService _employeeService;
        public EmployeeController(IEmployeeService employeeService)
        {
            _employeeService = employeeService;
        }
        // GET: api/<EmployeeController>
        [HttpGet]
        public List<Employee> Get()
        {
            return _employeeService.GetEmployeesDetails();
        }

        // GET api/<EmployeeController>/5
        [HttpGet("{id}")]
        public Employee Get(int id)
        {
            return _employeeService.GetStudentRecord(id);
        }

        // POST api/<EmployeeController>
        [HttpPost]
        public List<Employee> Post([FromBody] Employee employee)
        {
            return _employeeService.Save(employee);
        }

        // PUT api/<EmployeeController>/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE api/<EmployeeController>/5
        [HttpDelete("{id}")]
        public List<Employee> Delete(int id)
        {
            return _employeeService.Delete(id);
        }
    }
}
